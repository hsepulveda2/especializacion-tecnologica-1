document.addEventListener('DOMContentLoaded', function () {
    new Vue({
        el: '#appProductos',
        data: {
            // Input nombre
            nombre: '',
            // Input descripcion
            descripcion: '',
            // Input precio
            precio: '',
            // Ver o no ver el formulario de actualizar
            formActualizar: false,
            // La posición de tu lista donde te gustaría actualizar 
            idActualizar: -1,
            // Input nombre dentro del formulario de actualizar
            nombreActualizar: '',
            // Input descripcion dentro del formulario de actualizar
            descripcionActualizar: '',
             // Input precio dentro del formulario de actualizar
            precioActualizar: 0,
            // Lista de productos
            productos: [] 
        },
        methods: {
            crearProducto: function () {
                // Anyadimos a nuestra lista
                this.productos.push({
                    id: + new Date(),
                    nombre: this.nombre,
                    descripcion: this.descripcion,
                    precio: this.precio
                });
                // Vaciamos el formulario de añadir
                this.nombre = '';
                this.descripcion = '';
                this.precio = 0;
            },
            verFormActualizar: function (producto_id) {
                // Antes de mostrar el formulario de actualizar, rellenamos sus campos
                this.idActualizar = producto_id;
                this.nombreActualizar = this.productos[producto_id].nombre;
                this.descripcionActualizar = this.productos[producto_id].descripcion;
                this.precioActualizar = this.productos[producto_id].precio;
                // Mostramos el formulario
                this.formActualizar = true;
            },
            borrarProducto: function (producto_id) {
                // Borramos de la lista
                this.productos.splice(producto_id, 1);
            },
            guardarActualizacion: function (producto_id) {
                // Ocultamos nuestro formulario de actualizar
                this.formActualizar = false;
                // Actualizamos los datos
                this.productos[producto_id].nombre = this.nombreActualizar;
                this.productos[producto_id].descripcion = this.descripcionActualizar;
                this.productos[producto_id].precio = this.precioActualizar;
            }
        }
    });
});
