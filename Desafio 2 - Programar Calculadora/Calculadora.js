class Calculadora {
    sumar(n1, n2){
        return n1 + n2;
    }

    restar(n1, n2){
        return n1 - n2;
    }

    dividir(n1, n2){
        return n1 / n2;
    }

    multiplicar(n1, n2){
        return n1 * n2;
    }

    pasarANegativo(n1){
        return this.multiplicar(n1, -1);
    }
}
