class Display {
    constructor(displayNGuardado, displayOperacion, displayNActivo){
        this.displayNGuardado = displayNGuardado;
        this.displayOperacion = displayOperacion;
        this.displayNActivo = displayNActivo;
        this.calculadora = new Calculadora();
        this.nGuardado = '';
        this.operacion = '';
        this.nActivo = '';
    }

    agregarNumero(numero) {
        this.nActivo = this.nActivo + numero;
        this.imprimirValores();
    }

    imprimirValores(){
        this.displayNActivo.textContent = this.nActivo;
        this.displayOperacion.textContent = this.operacion;
        this.displayNGuardado.textContent = this.displayNGuardado;
    }
}
