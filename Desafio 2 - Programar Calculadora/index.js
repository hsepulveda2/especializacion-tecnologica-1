const displayNGuardado = document.getElementById("n-guardado");
const displayOperacion = document.getElementById("operacion");
const displayNActivo = document.getElementById("n-activo");
const botonesNumeros = document.querySelectorAll(".numero");
const botonesOperadores = document.querySelectorAll(".operador");
const display = new Display(displayNGuardado, displayOperacion, displayNActivo);

botonesNumeros.forEach(boton =>{
    boton.addEventListener('click', () =>{
        display.agregarNumero(boton.innerHTML);
    });
});
